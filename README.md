# CalenderViewer

## Overview
ブラウザにカレンダーとイベントを描画するアプリケーション  
イベントはEventAPIから取得  

## Description
 `/calendar/2017/10`でアクセスすると指定した月のカレンダーとイベントを描画する  
 描画したカレンダーのイベントを削除するボタンを実装  

### 言語とWAF
ruby + ruby on rails  

## Demo

## Licence

## Author
Shota Itabashi  
