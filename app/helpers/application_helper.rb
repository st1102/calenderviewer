# This helps application
module ApplicationHelper
  def shape_date(year, month, day, symbol)
    year + symbol + month + symbol + day
  end

  def shape_time(hour, min, symbol)
    hour + symbol + min
  end

  def join_string(*array)
    array.join
  end

  def form_error?
    !params[:year].match(/\d{4}/) || !params[:month].match(/\d{1,2}/) ||
      !params[:day].match(/\d{1,2}/) || !params[:start_hour].match(/\d{1,2}/) ||
      !params[:start_min].match(/\d{1,2}/) || !params[:end_hour].match(/\d{1,2}/) ||
      !params[:end_min].match(/\d{1,2}/) || params[:schedule] == ''
  end

  def return_create_uri
    URI.parse(join_string('http://10.10.10.10:4567/events/',
                          params[:year], '/', params[:month],
                          '?date=',
                          shape_date(params[:year],
                                     params[:month],
                                     params[:day], '-'),
                          '&start=',
                          shape_time(params[:start_hour],
                                     params[:start_min], ':'),
                          '&end=',
                          shape_time(params[:end_hour],
                                     params[:end_min], ':'),
                          '&schedule=',
                          params[:schedule]))
  end

  def return_update_uri
    URI.parse(join_string('http://10.10.10.10:4567/events/',
                          params[:id],
                          '?date=',
                          shape_date(params[:year],
                                     params[:month],
                                     params[:day], '-'),
                          '&start=',
                          shape_time(params[:start_hour],
                                     params[:start_min], ':'),
                          '&end=',
                          shape_time(params[:end_hour],
                                     params[:end_min], ':'),
                          '&schedule=',
                          params[:schedule]))
  end
end
