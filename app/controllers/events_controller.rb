class EventsController < ApplicationController
  include ApplicationHelper

  def show
    @year = params[:year]
    @month = params[:month]
    @months_eng = MONTHS_ENG

    uri = URI.parse('http://10.10.10.10:4567/events/' + @year + '/' + @month)
    request = Net::HTTP.get(uri)
    @response = JSON.parse(request)
  end

  def new
    @year = params[:year]
    @month = params[:month]
    @day = params[:day]
    @start_hour = params[:start_hour]
    @start_min = params[:start_min]
    @end_hour = params[:end_hour]
    @end_min = params[:end_min]
    @schedule = params[:schedule]
  end

  def create
    if form_error?
      then redirect_to new_event_path(year: params[:year], month: params[:month], day: params[:day],
                                      start_hour: params[:start_hour], start_min: params[:start_min],
                                      end_hour: params[:end_hour], end_min: params[:end_min],
                                      schedule: params[:schedule]),
                       flash: { notice: '不正な入力があります' }
    else
      Net::HTTP.new('10.10.10.10', '4567')
               .request(Net::HTTP::Post.new(return_create_uri))

      redirect_to show_event_path(params[:year], params[:month])
    end
  end

  def edit
    @id = params[:id]
    @array_date = params[:date].split('-')
    @array_start = params[:start].split('T')[1].split(':')
    @array_end = params[:end].split('T')[1].split(':')
    @schedule = params[:schedule]
  end

  def update
    if form_error?
      then redirect_to edit_event_path(id: params[:id],
                                       date: shape_date(params[:year], params[:month], params[:day], '-'),
                                       start: 'T' + shape_time(params[:start_hour], params[:start_min], ':'),
                                       end: 'T' + shape_time(params[:end_hour], params[:end_min], ':'),
                                       schedule: params[:schedule]),
                       flash: { notice: '不正な入力があります' }
    else
      Net::HTTP.new('10.10.10.10', '4567')
               .request(Net::HTTP::Put.new(return_update_uri))

      redirect_to show_event_path(params[:year], params[:month])
    end
  end

  def delete
    Net::HTTP.new('10.10.10.10', '4567')
             .request(Net::HTTP::Delete.new(URI.parse('http://10.10.10.10:4567/events/' + params[:id])))
    redirect_to show_event_path(params[:year], params[:month])
  end
end
