# This controlls application actions
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  MONTHS_ENG = {
    '1' => 'JANUARY', '2' => 'FEBRUARY', '3' => 'MARCH', '4' => 'APRIL', '5' => 'MAY', '6' => 'JUNE',
    '7' => 'JULY', '8' => 'AUGUST', '9' => 'SEPTEMBER', '10' => 'OCTOBER', '11' => 'NOVEMBER', '12' => 'DECEMBER'
  }.freeze
end
