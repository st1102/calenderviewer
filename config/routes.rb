Rails.application.routes.draw do
  get '/calendar/:year/:month', to: 'events#show', as: 'show_event'
  get '/event/new', to: 'events#new', as: 'new_event'
  post '/event', to: 'events#create', as: 'create_event'
  get '/event/:id/edit', to: 'events#edit', as: 'edit_event'
  patch '/event/:id', to: 'events#update', as: 'update_event'
  put '/event/:id', to: 'events#update'

  delete '/event/:id', to: 'events#delete', as: 'delete_event'
end
