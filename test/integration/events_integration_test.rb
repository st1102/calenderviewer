require 'test_helper'
require 'webmock'

include WebMock::API
include ApplicationHelper

class EventsControllerTest < ActionController::TestCase
  include ApiStub

  test 'should create event' do
    WebMock.enable!

    uri = URI.parse('http://10.10.10.10:4567/events/2017/10')
    request = Net::HTTP.get(uri)
    response = JSON.parse(request)

    assert_response 200

    get :show, params: { year: '2017', month: '10' }
    assert_response :success

    get :edit, params: { id: response[1]['id'], date: response[1]['date'],
                         start: response[1]['start'], end: response[1]['end'], schedule: response[1]['schedule'] }
    assert_response :success

    uri = URI.parse('http://10.10.10.10:4567/events/2017/11?date=2017-11-1&end=18:00&schedule=event&start=12:30')
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri)
    http.request(request)
    assert_response 200

    get :create, params: { year: '2017', month: '11', day: '1',
                           start_hour: '12', start_min: '30',
                           end_hour: '18', end_min: '00',
                           schedule: 'event' }
    assert_response :redirect
    assert_redirected_to action: 'show',
                         year: '2017',
                         month: '11'
  end

  test 'should update event' do
    WebMock.enable!

    response = JSON.parse(Net::HTTP.get(URI.parse('http://10.10.10.10:4567/events/2017/10')))
    assert_response 200

    get :show, params: { year: '2017', month: '10' }
    assert_response :success

    get :edit, params: { id: response[1]['id'], date: response[1]['date'], start: response[1]['start'],
                         end: response[1]['end'], schedule: response[1]['schedule'] }
    assert_response :success

    uri = URI.parse(join_string('http://10.10.10.10:4567/events/', response[1]['id'].to_s,
                                '?date=', response[1]['date'],
                                '&start=', response[1]['start'].split('T')[1].split(':')[0],
                                ':', response[1]['start'].split('T')[1].split(':')[1],
                                '&end=', response[1]['end'].split('T')[1].split(':')[0],
                                ':', response[1]['end'].split('T')[1].split(':')[1], '&schedule=', 'new event'))
    Net::HTTP.new(uri.host, uri.port).request(Net::HTTP::Put.new(uri))
    assert_response 200

    get :update, params: { id: response[1]['id'], year: response[1]['date'].split('-')[0],
                           month: response[1]['date'].split('-')[1], day: response[1]['date'].split('-')[2],
                           start_hour: response[1]['start'].split('T')[1].split(':')[0],
                           start_min: response[1]['start'].split('T')[1].split(':')[1],
                           end_hour: response[1]['end'].split('T')[1].split(':')[0],
                           end_min: response[1]['end'].split('T')[1].split(':')[1], schedule: 'new event' }
    assert_response :redirect
    assert_redirected_to action: 'show', year: response[1]['date'].split('-')[0],
                         month: response[1]['date'].split('-')[1]
  end

  test 'should delete event' do
    WebMock.enable!

    uri = URI.parse('http://10.10.10.10:4567/events/2017/10')
    request = Net::HTTP.get(uri)
    response = JSON.parse(request)
    assert_response 200

    get :show, params: { year: '2017', month: '10' }
    assert_response :success

    get :edit, params: { id: response[1]['id'],
                         date: response[1]['date'],
                         start: response[1]['start'],
                         end: response[1]['end'],
                         schedule: response[1]['schedule'] }
    assert_response :success

    uri = URI.parse('http://10.10.10.10:4567/events/' + response[1]['id'].to_s)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Delete.new(uri)
    http.request(request)
    assert_response 200

    get :delete, params: { id: response[1]['id'],
                           year: response[1]['date'].split('-')[0],
                           month: response[1]['date'].split('-')[1] }
    assert_response :redirect
    assert_redirected_to action: 'show',
                         year: response[1]['date'].split('-')[0],
                         month: response[1]['date'].split('-')[1]
  end
end
