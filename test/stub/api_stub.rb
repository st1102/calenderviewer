require 'test_helper'
require 'webmock'

include WebMock::API
include ApplicationHelper

module ApiStub
  WebMock.enable!

  uri = URI.parse('http://10.10.10.10:4567/events/2017/10')
  stub_request(:get, uri)
    .to_return(body: File.read(Rails.root.join('test', 'fixtures', 'stub_api_response.json')),
               status: 200,
               headers: { 'Content-Type' => 'application/json' })

  uri = URI.parse('http://10.10.10.10:4567/events/2017/11?date=2017-11-1&end=18:00&schedule=event&start=12:30')
  stub_request(:post, uri)
    .to_return(headers: { 'Content-Type' => 'application/json' })

  uri = URI.parse(join_string('http://10.10.10.10:4567/events/',
                              '42',
                              '?date=',
                              '2017-10-05',
                              '&start=',
                              '13:00',
                              '&end=',
                              '18:00',
                              '&schedule=',
                              'new event'))
  stub_request(:put, uri)
    .to_return(headers: { 'Content-Type' => 'application/json' })

  uri = URI.parse('http://10.10.10.10:4567/events/' + '42')
  stub_request(:delete, uri)
    .to_return(headers: { 'Content-Type' => 'application/json' })
end
