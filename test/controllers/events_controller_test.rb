require 'test_helper'

class EventsControllerTest < ActionController::TestCase
  def setup
    @event = events(:one)
  end

  test 'should display events' do
    WebMock.disable!
    get :show, params: { year: '2017', month: '10' }
    assert_response :success
    assert_equal '2017', assigns(:year)
    assert_equal '10', assigns(:month)
    assert_template :show
    assert_template layout: 'layouts/application'
  end

  test 'should get new' do
    get :new
    assert_response :success
    assert_template :new
    assert_template layout: 'layouts/application'
  end

  test 'should create events' do
    WebMock.enable!
    get :create, params: { year: '2017',
                           month: '11',
                           day: '1',
                           start_hour: '12',
                           start_min: '30',
                           end_hour: '18',
                           end_min: '00',
                           schedule: 'event' }
    assert_response :redirect
    assert_redirected_to action: 'show', year: '2017', month: '11'
  end

  test 'should get edit' do
    get :edit, params: { id: @event.id,
                         date: @event.date,
                         start: @event.start,
                         end: @event.end,
                         schedule: @event.schedule }

    assert_equal @event.id, assigns(:id).to_i
    assert_equal @event.schedule, assigns(:schedule)

    assert_response :success
    assert_template :edit
    assert_template layout: 'layouts/application'
  end

  test 'should update events' do
    WebMock.disable!
    get :update, params: { id: @event.id,
                           year: '2017',
                           month: '11',
                           day: '1',
                           start_hour: '12',
                           start_min: '30',
                           end_hour: '18',
                           end_min: '00',
                           schedule: 'internship' }
    assert_response :redirect
    assert_redirected_to action: 'show', year: '2017', month: '11'
  end

  test 'should get delete' do
    WebMock.disable!
    get :delete, params: { id: @event.id,
                           year: @event.date.year,
                           month: @event.date.month }
    assert_response :redirect
    assert_redirected_to action: 'show', year: @event.date.year, month: @event.date.month
  end
end
