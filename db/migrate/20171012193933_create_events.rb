class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.date :date
      t.time :start
      t.time :end
      t.string :schedule
    end
  end
end
